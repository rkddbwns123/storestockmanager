package com.kyjg.storestockmanager.controller;

import com.kyjg.storestockmanager.model.StoreStockItem;
import com.kyjg.storestockmanager.model.StoreStockPriceUpdateRequest;
import com.kyjg.storestockmanager.model.StoreStockRequest;
import com.kyjg.storestockmanager.service.StoreStockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store-stock")
public class StoreStockController {
    private final StoreStockService storeStockService;

    @PostMapping("/data")
    public String setStoreStock(@RequestBody @Valid StoreStockRequest request) {
        storeStockService.setStoreStock(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<StoreStockItem> getCustomers() {
        List<StoreStockItem> result = storeStockService.getCustomers();

        return result;
    }

    @PutMapping("/price/id/{id}")
    public String putPrice(@PathVariable long id, @RequestBody @Valid StoreStockPriceUpdateRequest request) {
        storeStockService.putPrice(id, request);

        return "OK";
    }

    @DeleteMapping("/del-data/id/{id}")
    public String delStoreStock(@PathVariable long id) {
        storeStockService.delStoreStock(id);

        return "OK";
    }
}
