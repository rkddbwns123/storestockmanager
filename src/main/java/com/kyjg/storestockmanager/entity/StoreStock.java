package com.kyjg.storestockmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class StoreStock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 30)
    private String productName;
    @Column(nullable = false)
    private Integer sellingPrice;
    @Column(nullable = false)
    private Integer stockAmount;
    @Column(nullable = false)
    private LocalDate manufacturingDate;
    @Column(nullable = false)
    private Integer expirationDate;

    private LocalDate wasteDate;
}
