package com.kyjg.storestockmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StoreStockItem {
    private Long id;

    private String productName;

    private Integer sellingPrice;

    private Integer stockAmount;

    private LocalDate manufacturingDate;

    private LocalDate wasteDate;

    private String productStatus;
}
