package com.kyjg.storestockmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreStockPriceUpdateRequest {
    private Integer sellingPrice;
}
