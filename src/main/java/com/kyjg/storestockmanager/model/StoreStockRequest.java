package com.kyjg.storestockmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class StoreStockRequest {
    @NotNull
    @Length(min = 1, max = 30)
    private String productName;
    @NotNull
    @Min(value = 100)
    @Max(value = 1000000)
    private Integer sellingPrice;
    @NotNull
    @Min(value = 1)
    @Max(value = 1000)
    private Integer stockAmount;
    private Integer expirationDate;
    @NotNull
    private LocalDate manufacturingDate;


}
