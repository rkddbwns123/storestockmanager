package com.kyjg.storestockmanager.repository;

import com.kyjg.storestockmanager.entity.StoreStock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreStockRepository extends JpaRepository<StoreStock, Long> {
}
