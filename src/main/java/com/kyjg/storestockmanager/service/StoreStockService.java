package com.kyjg.storestockmanager.service;

import com.kyjg.storestockmanager.entity.StoreStock;
import com.kyjg.storestockmanager.model.StoreStockItem;
import com.kyjg.storestockmanager.model.StoreStockPriceUpdateRequest;
import com.kyjg.storestockmanager.model.StoreStockRequest;
import com.kyjg.storestockmanager.repository.StoreStockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreStockService {
    private final StoreStockRepository storeStockRepository;

    public void setStoreStock(StoreStockRequest request) {
        StoreStock addData = new StoreStock();

        addData.setProductName(request.getProductName());
        addData.setSellingPrice(request.getSellingPrice());
        addData.setStockAmount(request.getStockAmount());
        addData.setManufacturingDate(request.getManufacturingDate());
        addData.setExpirationDate(request.getExpirationDate());
        addData.setWasteDate(request.getManufacturingDate().plusDays(request.getExpirationDate()));

        storeStockRepository.save(addData);
    }

    public List<StoreStockItem> getCustomers() {
        List<StoreStock> originList = storeStockRepository.findAll();

        List<StoreStockItem> result = new LinkedList<>();

        for (StoreStock item : originList) {
            StoreStockItem addItem = new StoreStockItem();

            addItem.setId(item.getId());
            addItem.setProductName(item.getProductName());
            addItem.setSellingPrice(item.getSellingPrice());
            addItem.setStockAmount(item.getStockAmount());
            addItem.setManufacturingDate(item.getManufacturingDate());
            addItem.setWasteDate(item.getWasteDate());

            String statusName = "정상";
            if (item.getWasteDate().isBefore(LocalDate.now())) {
                statusName = "폐기";
            } else if (item.getWasteDate().minusDays(1).equals(LocalDate.now())) {
                statusName = "임박";
            } else if (item.getWasteDate().equals(LocalDate.now())) {
                statusName = "임박";
            }

            addItem.setProductStatus(statusName);

            result.add(addItem);
        }
        return result;
    }

    public void putPrice(long id, StoreStockPriceUpdateRequest request) {
        StoreStock originData = storeStockRepository.findById(id).orElseThrow();

        originData.setSellingPrice(request.getSellingPrice());

        storeStockRepository.save(originData);
    }

    public void delStoreStock(long id) {
        storeStockRepository.deleteById(id);
    }
}
